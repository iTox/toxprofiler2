#' tfp server function
#'
#' @param input input
#' @param output output
#' @param session session
#'
#' @return creates the server part of the tfp browser
#'
tfp_server = function(input, output, session) {
  # request 4GB memory
  options(shiny.maxRequestSize = 8000 * 1024 ^ 2)
  
  
  
  # get session data for plot scaling
  cdata = session$clientData

  grid <- dbGetQuery(con, "SELECT * FROM grid")
  permissions <- dbGetQuery(con, "SELECT * FROM permissions")
  targets_all <- dbGetQuery(con, "SELECT * FROM targets_all")
  
  
  ## passwords etc -----------------------------------------------------------
  # groups that are not pw protected
  open_groups = setdiff(targets_all$group, permissions$group) %>% unique()
  
  # the target df is filtered according to pw input
  targets = eventReactive(input$password_button, {
    pw_check_vector = sapply(unique(permissions$password), function(hash) {
      bcrypt::checkpw(input$password_input, hash)
    })
    
    if (length(pw_check_vector) == 0) {
      message('No password needed')
      targets_all %>% filter(group %in% open_groups)
    } else if (sum(pw_check_vector) >= 1) {
      message('Login successful')
      # if this sum is >= 1 the input password corresponds to one or more of the hashes, i.e. is a correct password
      unlocked_groups = permissions$group[pw_check_vector]
      targets_all %>% filter(group %in% c(unlocked_groups, open_groups))
    } else {
      message('Login unsuccessful')
      targets_all %>% filter(group %in% open_groups)
    }
  }, ignoreNULL = F)
  
  pwstatus = eventReactive(input$password_button, {
    pw_check_vector = sapply(unique(permissions$password), function(hash) {
      bcrypt::checkpw(input$password_input, hash)
    })
    if (sum(pw_check_vector) >= 1) {
      return('Login successful!')
    } else {
      return('Wrong password')
    }
  })
  
  output$pw_status = pwstatus
  
  output$permission_info = DT::renderDataTable({
    DT::datatable(
      data = targets_all %>% as.data.frame() %>% group_by(group, url, substance, model) %>%
        summarise(
          maps = length(substance),
          concentrations = n_distinct(concentration_level),
          time_points = n_distinct(time_hpe)) %>%
        ungroup() %>%
        group_by(group, url) %>%
        summarise(
          substances = paste(substance, collapse = ', '),
          models = paste(unique(model), collapse = ', '),
          maps = sum(maps)
        ) %>%
        mutate(url = if_else(is.na(url),
                             'dataset not published',
                             sprintf('<a href="%s" target="_blank">%s</a>', url, url)
        )) %>%
        add_column(password_protected = if_else(.$group %in% permissions$group, 'yes', 'no')) %>%
        add_column(unlocked = if_else(.$group %in% targets()$group, 'yes', 'no')) %>%
        arrange(password_protected, group, substances),
      colnames = c(
        'Data source',
        'DOI',
        'Compound(s)',
        'Model(s)',
        'No. of maps',
        'Protected',
        'Unlocked'
      ),
      options = list(
        pageLength = 20
      ),
      escape = F
    ) %>% DT::formatStyle(
      columns = 'unlocked',
      background = DT::styleEqual(levels = c('yes', 'no'), values = c('#66cc00', '#ff6666'))
    )
  }, server = F)
  
  # Initialize a reactive value to track updates
  values <- reactiveValues(updateSource = NULL)
  
  # Reactive variables to store current selections
  currentSubstance <- reactiveVal("")
  currentGroup <- reactiveVal("")
  
  # Reactive expressions for filtered choices
  filteredSubstances <- reactive({
    if (!is.null(input$selectedgroup) && nzchar(input$selectedgroup)) {
      sort(unique(targets()$substance[targets()$group == input$selectedgroup]))
    } else {
      sort(unique(targets()$substance))
    }
  })
  
  filteredGroups <- reactive({
    if (!is.null(input$selectedsubstance1) && nzchar(input$selectedsubstance1)) {
      sort(unique(targets()$group[targets()$substance == input$selectedsubstance1]))
    } else {
      sort(unique(targets()$group))
    }
  })
  
  # Substance choice UI
  output$substancechoice = renderUI({
    selectizeInput("selectedsubstance1", "Compound", choices = c("Select a compound" = "", filteredSubstances()),
                   selected = currentSubstance(),
                   options = list(placeholder = 'Please choose a compound'))
  })
  
  # Group choice UI
  output$groupchoice = renderUI({
    selectizeInput("selectedgroup", "Data source", choices = c("Select a data source" = "", filteredGroups()),
                   selected = currentGroup(),
                   options = list(placeholder = 'Please choose a data source'))
  })
  
  # Observe event for compound selection
  observeEvent(input$selectedsubstance1, {
    values$updateSource <- "substance"
    currentSubstance(input$selectedsubstance1)  # Store the selected substance
    
    newFilteredGroups <- filteredGroups()  # Get the new filtered groups
    updateSelectInput(session, "selectedgroup", choices = c("", newFilteredGroups), selected = currentGroup())
  }, ignoreNULL = FALSE)
  
  # Observe event for group selection
  observeEvent(input$selectedgroup, {
    values$updateSource <- "group"
    currentGroup(input$selectedgroup)  # Store the selected group
    
    newFilteredSubstances <- filteredSubstances()  # Get the new filtered substances
    updateSelectInput(session, "selectedsubstance1", choices = c("", newFilteredSubstances), selected = currentSubstance())
  }, ignoreNULL = FALSE)
  # Inside your server function
  observeEvent(input$reset, {
    # Reset the current selections
    currentSubstance("")
    currentGroup("")
    
    # Reset the substance and group choices to show all options
    updateSelectInput(session, "selectedsubstance1", choices = c("", sort(unique(targets()$substance))), selected = "")
    updateSelectInput(session, "selectedgroup", choices = c("", sort(unique(targets()$group))), selected = "")
  })
  
  ## condition choice --------------------------------------------------------------------------
  output$conditionchoice = renderUI({
    argsselector =
      list(
        inputId = "selectedcondition1",
        label = "Additional condition",
        choices = as.list(unique(
          targets()$condition[targets()$group == input$selectedgroup &
                                targets()$substance == input$selectedsubstance1]
        )),
        selected = 1
      )
    
    selector =
      do.call('selectizeInput', argsselector)
    selector
  })
  
  
  ## make concentration slider ---------------------------------------------------------
  
  current_concenctrations = reactive({
    group_i = req(input$selectedgroup, cancelOutput = T)
    substance_i = req(input$selectedsubstance1, cancelOutput = T)
    condition_i = req(input$selectedcondition1, cancelOutput = T)
    
    concentrations =
      targets() %>%
      arrange(concentration) %>%
      filter(group == group_i &
               substance == substance_i)
    # if condition is NA it will be converted to 'NA' (character) by the dropdown menu function
    if (!condition_i == 'NA') {
      concentrations = concentrations %>%
        filter(condition == condition_i)
    }
    
    concentrations = concentrations %>%
      pull(concentration_level) %>%
      unique()
    concentrations = as.character(concentrations)
    concentrations
  })
  output$concslider = renderUI({
    concentrations = req(current_concenctrations())
    sliderTextInput(
      inputId = 'concselect1',
      label = 'Select concentration level:',
      choices = concentrations,
      selected = concentrations[length(concentrations)],
      grid = T
    )
  })
  
  
  ## make time point slider ------------------------------------------------------------
  
  current_times = reactive({
    group_i = req(input$selectedgroup, cancelOutput = T)
    substance_i = req(input$selectedsubstance1, cancelOutput = T)
    condition_i = req(input$selectedcondition1, cancelOutput = T)
    
    
    times =
      targets() %>%
      arrange(time_hpe) %>%
      filter(group == group_i &
               substance == substance_i)
    # if condition is NA it will be converted to 'NA' (character) by the dropdown menu function
    if (!condition_i == 'NA') {
      times = times %>%
        filter(condition == condition_i)
    }
    times = times %>%
      pull(time_hpe) %>%
      unique()
    times = as.character(times)
    times
  })
  output$timeslider = renderUI({
    times = req(current_times())
    
    sliderTextInput(
      inputId = 'timeselect1',
      label = 'Time [hpe]:',
      choices = times,
      selected = times[length(times)],
      grid = T
    )
  })
  
  ## select node (map) ------------------------------------------------------------------------
  nodecoord = reactiveValues(x = 0, y = 0)
  
  # cluster_table = reactiveValues(cluster_table = NA)
  
  click_data = reactive({
    d = event_data("plotly_click")
    if (is.null(d)) {
      d = list(x = 0, y = 0)
    }
    d
  })
  
  observeEvent(click_data(), {
    nodecoord$x = click_data()$x
    nodecoord$y = click_data()$y
  })
  
  ## gene selection (list) ---------------------------------------------------------------------
  output$genesearcher = renderUI({
    # Query gene names and node numbers from the PostgreSQL database
    query <- "SELECT external_gene_name, node FROM nodeannotation"
    gene_data <- dbGetQuery(con, query)
    
    # Create the select input choices
    selectList <- data.frame(genenames = gene_data$external_gene_name, node = gene_data$node)
    
    argssearcher <- list(
      inputId = "geneselect",
      label = "Search for gene on the map:",
      choices = c(Choose = '', selectList),
      selected = NULL,
      multiple = FALSE
    )
    
    searcher <- do.call('selectizeInput', argssearcher)
    searcher
  })
  observeEvent(input$geneselect, {
    if (is.null(input$geneselect)) {
      nodecoord$x = 0
      nodecoord$y = 0
    } else {
      # Query the PostgreSQL database to get the node corresponding to the selected gene
      query <- paste0("SELECT node FROM nodeannotation WHERE external_gene_name = '", input$geneselect, "'")
      node_data <- dbGetQuery(con, query)
      
      if (nrow(node_data) > 0) {
        # If a node is found, update node coordinates
        n <- node_data$node[1]
        nodecoord$x <- grid$x[n]
        nodecoord$y <- grid$y[n]
      } else {
        # If no node is found, set coordinates to 0
        nodecoord$x <- 0
        nodecoord$y <- 0
      }
    }
  })
  
  
  
  ## node selection (list) ---------------------------------------------------------------------
  output$nodeselector = renderUI({
    # Query the PostgreSQL database to get node data
    query <- "SELECT DISTINCT node FROM nodeannotation ORDER BY node"
    node_data <- dbGetQuery(con, query)
    
    # Create select list
    selectList <- data.frame(
      node = node_data$node,
      nodenr = node_data$node
    )
    
    argssearcher <- list(
      inputId = "nodeselect",
      label = "Select Node-Nr.:",
      choices = c(Choose = '', selectList),
      selected = NULL,
      multiple = FALSE
    )
    
    searcher <- do.call('selectizeInput', argssearcher)
    searcher
  })
  
  observeEvent(input$nodeselect, {
    if (is.null(input$nodeselect)) {
      nodecoord$x = 0
      nodecoord$y = 0
    } else{
      nodecoord$x = grid$x[as.numeric(input$nodeselect)]
      nodecoord$y = grid$y[as.numeric(input$nodeselect)]
    }
  })
  
  ## get map legend ------------------------------------------------------------
  observeEvent(session, {
    current_selection = reactive({
      concentrations =
        req(sort(unique(targets()$concentration_level[targets()$group == input$selectedgroup &
                                                        targets()$substance == input$selectedsubstance1])))
      concentrations = as.character(concentrations)
      times =
        req(sort(unique(targets()$time_hpe[targets()$group == input$selectedgroup &
                                             targets()$substance == input$selectedsubstance1])))
      times = as.character(times)
      
      conc_id = which(concentrations == input$concselect1)
      time_id = which(times == input$timeselect1)
      # check if concentration was tested at that timepoint
      
      selection = (
        targets()$group == input$selectedgroup &
          targets()$substance == input$selectedsubstance1 &
          # the condition is optional in a dataset, so sometimes
          # it is NA. If it is NA, it will be converted to 'NA'
          # (character) by the dropdown menu function. If it is
          # 'NA' TRUE will be returned as there are no conditions
          # to choose from. If it is not 'NA' the selected
          # condition is checked against the possible conditions
          {if (!input$selectedcondition1 == 'NA'){targets()$condition == input$selectedcondition1} else {T}} &
          targets()$concentration_level == sort(unique(targets()$concentration_level[targets()$substance == input$selectedsubstance1 &
                                                                                       targets()$group == input$selectedgroup]))[conc_id] &
          targets()$time_hpe == sort(unique(targets()$time_hpe[targets()$substance == input$selectedsubstance1 &
                                                                 targets()$group == input$selectedgroup]))[time_id]
      )
      message(sprintf('%i conditions/ maps meet the current selection of variables', sum(selection)))
      selection
    })
    
    current_map = reactive({
      if (sum(current_selection()) == 1) {
        map_name = targets()$name[current_selection()]
        message(sprintf('Only one map meets the selection criteria. Selected map: %s', map_name))
        return(map_name)
      } else {
        NULL
      }
    }, label = 'current_map')
    
    output$maplegend = renderPlot({
      map_name <- req(current_map())
      message(sprintf('Getting map legend for map %s', map_name))
      
      mapplot_data <- tbl(con, 'mapplot_data') %>%
        filter(mapID == map_name) %>%
        as.data.frame()
      
      # Get legend from mapplot_data
      legend_plot <- get_legend(mapplot_data) +
        theme(plot.margin = margin(0, 0, 0, 0, "cm"))
      
      # Plot legend
      print(legend_plot)
    }, height = 30, width =  min(cdata$output_plot1_width, 500))
    
    # plot map
    output$plot1 = renderPlotly({
      if (is.null(currentSubstance()) || currentSubstance() == "" || 
          is.null(currentGroup()) || currentGroup() == "") {
        return(generic_plot("Please make a selection"))  # Show a default message or view
      }
      if (is.null(input$concselect1) | is.null(input$timeselect1)) {
        generic_plot('Please select treatment')
      } else {
        if (sum(current_selection()) < 1) {
          generic_plot(
            'This concentration was not measured\n at the selected timepoint.\n\n Please select different concentration or timepoint.'
          )
        } else if (sum(current_selection()) == 1) {
          map_name <- req(current_map())
          message(sprintf('Plotting selected map: %s', map_name))
          
          # Retrieve map data
          map_data <- tbl(con, 'mapplot_data') %>%
            filter(mapID == map_name) %>%
            as.data.frame()
          
          # Plot SOM
          som_plot <- plot_SOM(map_data)
          
          # Optionally add coordinate highlighting
          highlighted_plot <- add_coord(som_plot, nodecoord$x, nodecoord$y)
          
          # Convert to plotly
          plotly_output <- som_to_plotly(highlighted_plot, cdata)
          
          plotly_output
        }
      }
    })
    
    
    output$mapheight = reactive({
      cdata$output_plot1_width
    })
    
    output$fingerprintheader = renderText({
      sprintf(
        'Transcriptomic response for %s at %.2f %s and %s hpe',
        req(input$selectedsubstance1),
        req(targets()$concentration[
          targets()$group == input$selectedgroup &
            targets()$substance == input$selectedsubstance1 &
            targets()$concentration_level == input$concselect1][1]),
        req(targets()$unit[
          targets()$group == input$selectedgroup &
            targets()$substance == input$selectedsubstance1 &
            targets()$concentration_level == input$concselect1][1] %>% convert_unit()),
        req(input$timeselect1)
      )
    })
    
    # plot node measurements --------------------------------------------------------------
    output$nodeplot = renderPlot({
      if (is.na(req(nodecoord$x)) | req(nodecoord$x) == 0) {
        # no node selected
        generic_plot('Click on fingerprint to select node')
      } else {
        # node selected, plot is made
        nodenr = which(grid$x == req(nodecoord$x) &
                         grid$y == req(nodecoord$y))
        selected_substance = req(input$selectedsubstance1)
        selected_group = req(input$selectedgroup)
        selected_condition = req(input$selectedcondition1)
        
        unit = req(targets()$unit[targets()$group == selected_group &
                                    targets()$substance == selected_substance &
                                    targets()$concentration_level == input$concselect1][1] %>% convert_unit())
        
        D_measured = tbl(con, 'D_measured_all') %>%
          filter(group == selected_group &
                   substance == selected_substance &
                   node == nodenr &
                   condition == selected_condition) %>%
          as.data.frame()
        
        D_fit = tbl(con, 'D_fit_all') %>%
          filter(group == selected_group &
                   substance == selected_substance &
                   node == nodenr &
                   condition == selected_condition) %>%
          as.data.frame()
        ControlCIs = tbl(con, 'Control_CIs_all') %>%
          filter(group == selected_group &
                   substance == selected_substance &
                   node == nodenr &
                   condition == selected_condition) %>%
          as.data.frame()
        
        plot_data = list(
          D_measured = D_measured,
          D_fit = D_fit,
          ControlCIs = ControlCIs,
          model = D_fit$model[1]
        )
        
        if (dim(D_measured)[1] > 0) {
          plot_noderesponse(plot_data = plot_data, conc_unit = unit)
        } else {
          # selected node has no data
          generic_plot('No measured data for selected node.\n Please select different node.')
        }
      }
    },
    # height = cdata$output_nodeplot_width
    height = min(cdata$output_plot1_height, 450)
    )
    
    
    # SOM summary -------------------------------------------------------------
    output$mapsummary = renderTable({
      map_name = req(current_map())
      tbl(con, 'mapplot_data') %>%
        filter(mapID == map_name) %>%
        as.data.frame() %>%
        SOM_summary()
    })
    
    
    # Node annotation table ----------------------------------------------------------------
    output$nodeInfo =
      DT::renderDataTable({
        if (is.na(nodecoord$x) | nodecoord$x == 0) {
          data.frame(Start = "Click on plot to select node")
        } else{
          nodenr =
            which(grid$x == nodecoord$x &
                    grid$y == nodecoord$y)
          DT::datatable(
            data = tbl(con, 'nodeannotation') %>% filter(node == nodenr) %>% select(
              ProbeID,
              ensembl_gene_id,
              external_gene_name,
              description,
              name_1006,
              interpro_description,
              gene_biotype
            ) %>% as.data.frame(),
            colnames = c(
              "ProbeID",
              "Ensembl gene-id",
              "Gene name",
              "Description",
              "GO Annotation",
              "Interpro description",
              "Gene biotype"
            )
          )
        }
      })
    
    ## nodeID as table header ----------------------------------------------------
    output$tableheader = renderText({
      if (is.na(nodecoord$x) | nodecoord$x == 0) {
        paste0("Gene table")
      } else{
        nodenr =
          which(grid$x == nodecoord$x &
                  grid$y == nodecoord$y)
        
        paste0("Gene table for node #", nodenr)
      }
    })
    
    ## publication info ----------------------------------------------------
    output$publication_doi = renderText({
      doi = req(targets()$url[targets()$name == current_map()])
      info = req(targets()$info[targets()$name == current_map()])
      if (is.na(doi)) {
        'This dataset is unpublished.'
      } else{
        sprintf('Publication: %s %s', info, sprintf('<a href="%s" target="_blank">%s</a>', doi, doi))
      }
    })
    
    
    
  })
}
