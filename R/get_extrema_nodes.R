#' Get extreme values across several experiments per node
#'
#' @param nodelist A list of nodes for each substance
#' @param dslist A list of ELists with normalized logFC values
#' @param keep_recovery Should recovery samples be included for the calculation of extrema?
#'
#' @return A dataframe with minimum, maximum and absolute extreme value for each node
#' @export
#'
get_extrema_nodes <- function(nodelist, elist, keep_recovery = FALSE) {
  library("pbapply")
  library("outliers")
  
  # Process each substance in the elist
  dslist_extrema <- pbapply::pblapply(X = seq(1, length(elist)), FUN = function(substanceID) {
    nodelist_for_substance <- nodelist[[substanceID]]
    
    # Determine maximum for each gene (median of condition) -------------------
    logFCagg <- lapply(X = nodelist_for_substance, FUN = function(node) {
      if (is.data.frame(node)) {
        
        # Remove outliers -------------------------------------------------
        conc <- node$concentration
        time <- node$time_hpe_factor
        timen <- node$time_hpe
        logFC <- node$logFC
        type <- node$type
        
        outliernew <- as.numeric(outliers::grubbs.test(x = as.numeric(logFC))["p.value"])
        
        while (log10(outliernew) < -3) {
          time <- time[-which(logFC == outliers::outlier(logFC))]
          timen <- timen[-which(logFC == outliers::outlier(logFC))]
          conc <- conc[-which(logFC == outliers::outlier(logFC))]
          logFC <- logFC[-which(logFC == outliers::outlier(logFC))]
          type <- type[-which(logFC == outliers::outlier(logFC))]
          outliernew <- as.numeric(outliers::grubbs.test(x = logFC)["p.value"])
        }
        
        # Calculate median of each condition and get extrema --------------
        df_agg <- aggregate.data.frame(x = logFC, by = list(conc = conc, time = time, type = type), FUN = median)
        maxnode <- max(df_agg$x)
        minnode <- min(df_agg$x)
        extnode <- maxnode
        extnode[abs(minnode) > maxnode] <- minnode[abs(minnode) > maxnode]
        result <- c(minnode, maxnode, extnode)
        return(result)
      } else {
        return(c(NA, NA, NA))
      }
    })
    
    extnodes <- as.data.frame(do.call(rbind, logFCagg))
    colnames(extnodes) <- c("min", "max", "extremum")
    return(extnodes)
  })
  
  minnodes <- do.call("cbind", lapply(dslist_extrema, function(nodelist) {
    nodelist$min
  }))
  maxnodes <- do.call("cbind", lapply(dslist_extrema, function(nodelist) {
    nodelist$max
  }))
  extnodes <- do.call("cbind", lapply(dslist_extrema, function(nodelist) {
    nodelist$extremum
  }))
  
  minnodes_all <- apply(minnodes, MARGIN = 1, FUN = min)
  maxnodes_all <- apply(maxnodes, MARGIN = 1, FUN = max)
  
  extnodes_all <- maxnodes_all
  extnodes_all[abs(minnodes_all) > maxnodes_all & !is.na(maxnodes_all)] <- minnodes_all[abs(minnodes_all) > maxnodes_all & !is.na(maxnodes_all)]
  
  extrema <- data.frame(min = minnodes_all, max = maxnodes_all, extremum = extnodes_all)
  return(extrema)
}
