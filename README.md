# toxprofileR2

This is a collection of R functions to create toxicogenomic fingerprints. This is the second version of the toxprofileR package, which aims to incorporate more functionalities. For the first version of toxprofileR as it is described in [Schüttler et al. 2019](https://doi.org/10.1093/gigascience/giz057) see [here](https://git.ufz.de/iTox/toxprofiler).


## Installation

To install the package, you need to install the package devtools first:

```r
install.packages("devtools")
```

Afterwards you can install toxprofileR2.

```r
devtools::install_git("https://git.ufz.de/itox/toxprofileR2.git")
```

Please note: There might be difficulties installing dependencies via Rstudio. If problems arise, please directly install package from R.


## Usage

### Importing data

You can import microarray or RNA-sequencing data using toxprofileR2, with the respective function below. Both functions will return a limma EList. As all modelling is done on log fold change data, this is also calculated.

```r
elist = import_array_data(...)
elist = import_seq_data(...)

elist_logfc = calc_logfc(elist)
```

### Modelling with linear models

....
```r
nodelist = create_nodelist_voom()

lm = fit_lm(nodelist, nodeframe)
```

## Contact

If there are issues with the package feel free to raise an issue here in gitlab.

## License

Something about the license

## Citation

How to cite
